<?php

namespace LightSource\FrontBlocksWebpackSample\Article;

use LightSource\FrontBlocks\Block;
use LightSource\FrontBlocksWebpackSample\CustomButton\CustomButton;

class Article extends Block
{

    protected string $name;
    protected CustomButton $button;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Article, I contain another block';
        $this->button->loadByTest();
    }
}
