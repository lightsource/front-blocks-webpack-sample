<?php

namespace LightSource\FrontBlocksWebpackSample\CustomButton;

use LightSource\FrontBlocks\Block;
use LightSource\FrontBlocksWebpackSample\Catalyst\Catalyst;

class CustomButton extends Block
{

    protected string $name;
    protected Catalyst $catalyst;

    public function loadByTest()
    {
        parent::load();
        $this->name = 'I\'m Button';
    }
}
